# coding: utf-8
def fleury_et_bott(rendu_monnaie):
    liste_argent = [['billet(s) de 500 euros', 500, 0], ['billet(s) de 200 euros', 200, 0],
                    ['billet(s) de 100 euros', 100, 0], ['billet(s) de 50 euros', 50, 0],
                    ['billet(s) de 20 euros', 20, 0], ['billet(s) de 10 euros', 10, 0],
                    ['billet(s) de 5 euros', 5, 0], ['pièce(s) de 2 euros', 2, 0],
                    ['pièce(s) de 1 euro', 1, 0]]
    for i in range(len(liste_argent)):
        while rendu_monnaie - liste_argent[i][1] >= 0:
            rendu_monnaie -= liste_argent[i][1]
            liste_argent[i][2] += 1
    return liste_argent


def madame_guipure(rendu_monnaie):
    liste_argent = [['billet(s) de 200 euros', 200, 0, 1], ['billet(s) de 100 euros', 100, 0, 3],
                    ['billet(s) de 50 euros', 50, 0, 1], ['billet(s) de 20 euros', 20, 0, 1],
                    ['billet(s) de 10 euros', 10, 0, 1], ['pièce(s) de 2 euros', 2, 0, 5]]
    if rendu_monnaie > 590:
        print("MADAME GUIPURE NE PEUT PAS REMBOURSER TOUT L'ARGENT, "
              "ELLE A EMPRUNTE CHEZ FLEURY ET BOTT.")
        for i in range(len(liste_argent)):
            while (rendu_monnaie - liste_argent[i][1] >= 0) and \
                  (liste_argent[i][3] > 0):
                rendu_monnaie -= liste_argent[i][1]
                liste_argent[i][2] += 1
                liste_argent[i][3] -= 1
        liste_secour = fleury_et_bott(rendu_monnaie)
        for argent in liste_secour:
            if argent[2] > 0:
                liste_argent.append(argent)
        return liste_argent, 0
    elif rendu_monnaie > 0:
        for i in range(len(liste_argent)):
            while (rendu_monnaie - liste_argent[i][1] >= 0) and \
                  (liste_argent[i][3] > 0):
                rendu_monnaie -= liste_argent[i][1]
                liste_argent[i][2] += 1
                liste_argent[i][3] -= 1
        if rendu_monnaie > 0:
            print("MADAME GUIPURE NE PEUT PAS REMBOURSER TOUT L'ARGENT,"
                  "ELLE REND PLUS.")
            plus_proche = 500
            for rendu_eventuel in liste_argent:
                if (rendu_eventuel[3] > 0) and (plus_proche > rendu_monnaie):
                    plus_proche = rendu_eventuel[1]
            return liste_argent, plus_proche
    return liste_argent, 0


def ollivander(rendu_monnaie):
    liste_argent_sorcier = [['Gallion(s), ', 493, 0], ['Mornille(s), ', 29, 0],
                            ['Noise(s)', 1, 0]]
    nombre_noise = liste_argent_sorcier[0][1] * rendu_monnaie[0]\
    + liste_argent_sorcier[1][1] * rendu_monnaie[1]\
    + liste_argent_sorcier[2][1] * rendu_monnaie[2]
    for i in range(len(rendu_monnaie)):
        liste_argent_sorcier[i][2] = nombre_noise // liste_argent_sorcier[i][1]
        nombre_noise %= liste_argent_sorcier[i][1]
    return liste_argent_sorcier


def triage_des_0(liste_a_trier):
    liste_trie = []
    for liste in liste_a_trier:
        if liste[2] > 0:
            liste_trie.append(liste)
    return liste_trie
    

rendu_argent_fleury = [0, 60, 63, 231, 899]
rendu_argent_guipure = [0, 8, 62, 231, 497, 842]
rendu_argent_ollivander = [[0, 0, 0], [0, 0, 654], [0, 23, 78],
                           [2, 11, 9], [7, 531, 451]]

for rendu in rendu_argent_fleury:
    total_fleury = 0
    plus_bas = 501
    liste_rendu_fleury = fleury_et_bott(rendu)
    for valeur in liste_rendu_fleury:
        total_fleury += valeur[2]
    for valeur in liste_rendu_fleury:
        if valeur[2] < 0:
            if plus_bas >= valeur[1]:
                plus_bas = valeur[1]
        elif plus_bas == 501:
            plus_bas = 1

    if total_fleury == 0:
        print("Fleury & Bott ne rend rien.")
    else:
        print("Fleury & Bott rend", end=' ')
        for i in range(len(liste_rendu_fleury)):
            if liste_rendu_fleury[i][2] != 0 and liste_rendu_fleury[i][1] > plus_bas:
                print(f"{liste_rendu_fleury[i][2]} {liste_rendu_fleury[i][0]}, ", end='')
            elif liste_rendu_fleury[i][1] == plus_bas:
                print(f"{liste_rendu_fleury[i][2]} {liste_rendu_fleury[i][0]}.")

for rendu in rendu_argent_guipure:
    total_guipure = 0
    plus_bas = 501
    liste_rendu_guipure, trop_rendu = madame_guipure(rendu)
    for valeur in liste_rendu_guipure:
        total_guipure += valeur[2]
    for valeur in liste_rendu_guipure:
        if valeur[2] > 0:
            if plus_bas >= valeur[1]:
                plus_bas = valeur[1]
    if total_guipure == 0:
        print("Madame Guipure ne rend rien.")
    else:
        print("Madame Guipure rend", end=' ')
        for i in range(len(liste_rendu_guipure)):
            if liste_rendu_guipure[i][2] != 0 and liste_rendu_guipure[i][1] != plus_bas:
                print(f"{liste_rendu_guipure[i][2]} {liste_rendu_guipure[i][0]}, ", end='')
        print(f"{liste_rendu_guipure[i][2]} {liste_rendu_guipure[i][0]}.")
        if trop_rendu != 0:
            print(f"Madame Guipure a donné {trop_rendu} euro supplémentaire pour "
                  f"rembourser Harry Potter.")
            
for rendu in rendu_argent_ollivander:
    total_ollivander = 0
    liste_rendu_ollivander = ollivander(rendu)
    for valeur in liste_rendu_ollivander:
        total_ollivander += valeur[2]
    if total_ollivander == 0:
        print("Ollivander ne rend rien.")
    else:
        print("Ollivander rend", end=' ')
        for liste in liste_rendu_ollivander:
            if liste[2] != 0:
                print(f"{liste[2]} {liste[0]}", end='')
        print(".")
  