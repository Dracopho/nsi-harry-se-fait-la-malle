# coding: utf-8
def fleury_et_bott(rendu_monnaie):
    '''
    la fonction sert à optimiser un rendu  monnaie
    en essayant de soustraire le plus gros billet ou pièce
    d'une valeur dans (la liste_argent)
    et puis de stocker le nombre de billets ou pièces
    pouvant être retirés dans une liste de liste
    pour pouvoir renvoyer une liste contenant le type des rendus
    billet ou pièce et sa valeur,
    la valeur stocké de ce billet ou pièce
    entrée :  un entier 'int'
    sortie : une liste 'list'
    '''
    liste_argent = [['billet(s) de 500 euros', 500, 0],
                    ['billet(s) de 200 euros', 200, 0],
                    ['billet(s) de 100 euros', 100, 0],
                    ['billet(s) de 50 euros', 50, 0],
                    ['billet(s) de 20 euros', 20, 0],
                    ['billet(s) de 10 euros', 10, 0],
                    ['billet(s) de 5 euros', 5, 0],
                    ['pièce(s) de 2 euros', 2, 0],
                    ['pièce(s) de 1 euro', 1, 0]]
    for y in range(len(liste_argent)):
        while rendu_monnaie - liste_argent[y][1] >= 0:
            rendu_monnaie -= liste_argent[y][1]
            liste_argent[y][2] += 1
    return liste_argent


def madame_guipure(rendu_monnaie):
    manque_argent = False
    liste_argent = [['billet(s) de 200 euros', 200, 0, 1],
                    ['billet(s) de 100 euros', 100, 0, 3],
                    ['billet(s) de 50 euros', 50, 0, 1],
                    ['billet(s) de 20 euros', 20, 0, 1],
                    ['billet(s) de 10 euros', 10, 0, 1],
                    ['pièce(s) de 2 euros', 2, 0, 5]]
    for i in range(len(liste_argent)):
        while (rendu_monnaie - liste_argent[i][1] >= 0) and \
            (liste_argent[i][3] > 0):
            rendu_monnaie -= liste_argent[i][1]
            liste_argent[i][2] += 1
            liste_argent[i][3] -= 1
    for i in range(len(liste_argent)):
        while (rendu_monnaie - liste_argent[i][1] >= 0) and \
            (liste_argent[i][3] > 0):
            rendu_monnaie -= liste_argent[i][1]
            liste_argent[i][2] += 1
            liste_argent[i][3] -= 1
        plus_proche = 500
        for rendu_eventuel in liste_argent:
            if (rendu_eventuel[3] > 0) and \
            (plus_proche > rendu_monnaie):
                plus_proche = rendu_eventuel[1]
    if rendu_monnaie > 0:
       manque_argent = True
    return liste_argent, manque_argent


def ollivander(rendu_monnaie):
    liste_argent_sorcier = [[' Gallion(s), ', 493, 0],
    [' Mornille(s), ', 29, 0],
    [' Noise(s) ', 1, 0]]
    nombre_noise = liste_argent_sorcier[0][1] * rendu_monnaie[0]\
    + liste_argent_sorcier[1][1] * rendu_monnaie[1]\
    + liste_argent_sorcier[2][1] * rendu_monnaie[2]
    for i in range(len(rendu_monnaie)):
        liste_argent_sorcier[i][2] = nombre_noise //\
            liste_argent_sorcier[i][1]
        nombre_noise %= liste_argent_sorcier[i][1]
    return liste_argent_sorcier


def affichage1(liste_des_sommes_a_rendre,probleme):
    for billet in liste_des_sommes_a_rendre:
        if billet[2] > 0:
            print(f" IL faut rendre {billet[2]} {billet[0]} ")
        
    if probleme :
        print(" MADAME GUIPURE NE PEUT PAS REMBOURSER TOUT L'ARGENT,"
            " ELLE REND PLUS. ")


def affichage2(liste_des_sommes_a_rendre_ollivanders):
    for billet in liste_des_sommes_a_rendre_ollivanders:
        if billet[2]> 0 :
            print(f"Mr Oliverders va rendre {billet[2]} {billet[0]}")
    if billet[2] == 0 :
        print(f"le rendu monnaie est impossible")
    
        
def menu() :
    choix_boutique = input(" Bienvenu au chemin de traverse, "
    "si vous voulez aller a la boutique de fleury et bots entrez ' a ' " 
    ",si vous voulez aller à la boutique de Madame Guipure entrez ' b ', "
    " si vous voulez aller dans la boutique de Mr Olivanders entrez ' c ' , "
    " sinon si vvous voulez quitter le chemin de traverse entrez ' q ' ")
    if choix_boutique == 'a':
        print(" bienvenu chez fleury et bott ")
        choix_fleury_et_bott1 = input(" Si vous voulez tester les valeurs"
        " ( 0€, 60 €,63 €, 231 € et 899 €.) entrez'a'\n "
        " , ou si vous voulez tester une valeur choisit entrez ' b ': "  )
        if choix_fleury_et_bott1 == 'a':
            liste_fleury_et_bott = [0,60,63,231,899]
            for i in range(len(liste_fleury_et_bott)):
                print(f" Pour rendre une somme de {liste_fleury_et_bott[i]} "
                " euros il faut :")
                affichage1(fleury_et_bott(liste_fleury_et_bott[i]),False)
        elif choix_fleury_et_bott1 == 'b':
            valeur_fleury_et_bott = int(input("quel nombre choissisez vous a rendre : "))
            print(f"pour rendre une somme de {valeur_fleury_et_bott} euros il faut : ")
            affichage1(fleury_et_bott(valeur_fleury_et_bott),False)
        menu()
    elif choix_boutique == 'b':
        print("bienvenu chez madame guipure")
        choix_fleury_et_bott2 = input(" Si vous voulez tester les valeurs"
        "( 0€, 8 €,62 €, 231 €, 497 € et 842 €) entrez ' a '  "
        " , ou si vous voulez tester une valeur choisit entrez ' b ' : "  )
        if choix_fleury_et_bott2 == 'a':
            liste_guipure = [0,8,62,231,497,842]
            for g in range(len(liste_guipure)):
                print(f" pour rendre une sommme de {liste_guipure[g]} euros ")
                affichage1(madame_guipure(liste_guipure[g])[0],madame_guipure(liste_guipure[g])[1])
        elif choix_fleury_et_bott2 == 'b':
            valeur_guipure = int(input(" quel nombre a rendre choisissez vous : "))
            print(f" pour rendre une sommme de {valeur_guipure} euros ")
            affichage1(madame_guipure(valeur_guipure)[0],madame_guipure(valeur_guipure)[1])
        menu()
    elif choix_boutique == 'c':
        print("bienvenu chez Mr Olivanders")
        choix_olivanders = input(" si vous voulez tester le rendu optimisé monnaie de "
        " (0 Noises,654 Noises,23 Mornilles et 78 Noises,"
        "2 Gallions 11 Mornilles et 9 Noises,7 Gallions,\n "
        " 531 Mornilles et 451 Noise) saisissez le ' a ' "
        " ou si vous voulez saisir un nombre choisit saisissez le ' b ' ")
        if choix_olivanders == 'a':
            nb_a_tester = [[0,0,0] , [0,0,654] , [0,23,78] , [2,11,9] , [7,531,451]]
            for i in range(len(nb_a_tester)):
                print(f" pour une valeur de {nb_a_tester[i][0]} galions"
                ", {nb_a_tester[i][1]} mornilles, {nb_a_tester[i][2]} "
                " noises,le vendeur va optimiser le rendu en : ")
                affichage2(ollivander(nb_a_tester[i]))
        elif choix_olivanders == 'b':
            liste_valeur_olivanders = [0,0,0]
            print(" vous aller donner 3 valeurs "
            "(le nombre de galion(s), le nombre de mornille(s),"
            " le nombre de noise(s)) ")
            galions = int(input(f' quel est le nombre de galion(s) '
            "que vous voulez optimiser ? : '"))
            mornilles = int(input(f' quel est le nombre de mornille(s)'
             "que vous voulez optimiser ? : '"))
            noises = int(input(f' quel est le nombre de noise(s)'
            " que vous voulez optimiser ? : "))
            liste_valeur_olivanders[0] = galions
            liste_valeur_olivanders[1] = mornilles
            liste_valeur_olivanders[2] = noises
            print(f" Mr Olivanders va vous  rendre une somme optimisé de"
            " {galions} galions, {mornilles} mornilles",
            "{noises} noises ")
            affichage2(ollivander(liste_valeur_olivanders))
        menu()


    else:
        print(" satisfait de vos achats? revenez bientôt ! ")

menu()
