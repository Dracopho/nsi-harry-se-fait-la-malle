# coding: utf-8
def fleury_et_bott(rendu_monnaie):
    liste_argent = [['billet de 500', 500, 0], ['billet de 200', 200, 0],
                    ['billet de 100', 100, 0], ['billet de 50', 50, 0],
                    ['billet de 20', 20, 0], ['billet de 10', 10, 0],
                    ['billet de 5', 5, 0], ['pièce de 2', 2, 0],
                    ['pièce de 1', 1, 0]]
    for i in range(len(liste_argent)):
        while rendu_monnaie - liste_argent[i][1] >= 0:
            rendu_monnaie -= liste_argent[i][1]
            liste_argent[i][2] += 1
    return liste_argent


def madame_guipure(rendu_monnaie):
    liste_argent = [['billet de 200', 200, 0, 1], ['billet de 100', 100, 0, 3],
                    ['billet de 50', 50, 0, 1], ['billet de 20', 20, 0, 1],
                    ['billet de 10', 10, 0, 1], ['pièce de 2', 2, 0, 5]]
    if rendu_monnaie > 590:
        print("MADAME GUIPURE NE PEUT PAS REMBOURSER TOUT L'ARGENT, "
              "ELLE A EMPRUNTE CHEZ FLEURY ET BOTT.")
        for i in range(len(liste_argent)):
            while (rendu_monnaie - liste_argent[i][1] >= 0) and \
                  (liste_argent[i][3] > 0):
                rendu_monnaie -= liste_argent[i][1]
                liste_argent[i][2] += 1
                liste_argent[i][3] -= 1
        liste_secour = fleury_et_bott(rendu_monnaie)
        for argent in liste_secour:
            if argent[2] > 0:
                liste_argent.append(argent)
        return liste_argent, 0
    elif rendu_monnaie > 0:
        for i in range(len(liste_argent)):
            while (rendu_monnaie - liste_argent[i][1] >= 0) and \
                  (liste_argent[i][3] > 0):
                rendu_monnaie -= liste_argent[i][1]
                liste_argent[i][2] += 1
                liste_argent[i][3] -= 1
        if rendu_monnaie > 0:
            print("MADAME GUIPURE NE PEUT PAS REMBOURSER TOUT L'ARGENT,"
                  "ELLE REND PLUS.")
            plus_proche = 500
            for rendu_eventuel in liste_argent:
                if (rendu_eventuel[3] > 0) and (plus_proche > rendu_monnaie):
                    plus_proche = rendu_eventuel[1]
            return liste_argent, plus_proche
    return liste_argent, 0


def ollivander(rendu_monnaie):
    liste_argent_sorcier = [['Gallion(s)', 493, 0], ['Mornille(s)', 29, 0],
                            ['Noise(s)', 1, 0]]
    nombre_noise = liste_argent_sorcier[0][1] * rendu_monnaie[0]
    + liste_argent_sorcier[1][1] * rendu_monnaie[1]
    + liste_argent_sorcier[2][1] * rendu_monnaie[2]
    for i in range(len(rendu_monnaie)):
        liste_argent_sorcier[i][2] = nombre_noise // liste_argent_sorcier[i][1]
        nombre_noise %= liste_argent_sorcier[i][1]
    return liste_argent_sorcier

# 1 gallion vaut 493 noises
rendu_monnaie = 842
rendu_monnaie_2 = [0, 0, 0]
trop_rendu = 0
liste_rendu_1 = []
liste_rendu = []
liste_rendu, trop_rendu = madame_guipure(rendu_monnaie)
liste_rendu_1 = fleury_et_bott(rendu_monnaie)
liste_rendu_sorcier = ollivander(rendu_monnaie_2)

for nombre in liste_rendu:
    print(f"Madame Guipure rend {nombre[2]} {nombre[0]} euros.")
if trop_rendu != 0:
    print(f"Madame Guipure a donné {trop_rendu} euro supplémentaire pour"
          f"rembourser Harry Potter.")
for nombre in liste_rendu_1:
    print(f"Fleury & Bott rend {nombre[2]} {nombre[0]} euros")
print(f"Ollivander rend {liste_rendu_sorcier[0][2]}"
      f" {liste_rendu_sorcier[0][0]},"
      f" {liste_rendu_sorcier[1][2]} {liste_rendu_sorcier[1][0]},"
      f" {liste_rendu_sorcier[2][2]} {liste_rendu_sorcier[2][0]}.")

# print("Très satisfait de votre visite, le patron vous invite à passer chez
# son voisin pour acheter une robe de sorcier.")

# prix_aleatoire1 = 0
# choix = input("pour exécuter le programe automatiquement répondez'oui'si vous
# référé l'exécuter manuellement repondez 'non'")
# if choix == 'oui':
#     prix_aleatoire = randint(1,1000)
#     print(f"le vendeur doit rendre une somme de {prix_aleatoire} euros")
# else :
#     prix_aleatoire = int(input("quel nombre a rendre choisissez vous : "))
#     print(f"le vendeur doit rendre une somme de {prix_aleatoire} euros")
