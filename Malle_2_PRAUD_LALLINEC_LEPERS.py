# coding: utf8

'''
Mini-projet "Harry se fait la malle"

Liste de fournitures scolaires 

Auteurs : Gabriel Praud, Corentin Lallinec, Aleksandr Lepers
'''

fournitures_scolaires = \
[{'Nom' : 'Manuel scolaire', 'Poids' : 0.55, 'Mana' : 11},
{'Nom' : 'Baguette magique', 'Poids' : 0.085, 'Mana' : 120},
{'Nom' : 'Chaudron', 'Poids' : 2.5, 'Mana' : 2},
{'Nom' : 'Boîte de fioles', 'Poids' : 1.2, 'Mana' : 4},
{'Nom' : 'Téléscope', 'Poids' : 1.9, 'Mana' : 6},
{'Nom' : 'Balance de cuivre', 'Poids' : 1.3, 'Mana' : 3},
{'Nom' : 'Robe de travail', 'Poids' : 0.5, 'Mana' : 8},
{'Nom' : 'Chapeau pointu', 'Poids' : 0.7, 'Mana' : 9},
{'Nom' : 'Gants', 'Poids' : 0.6, 'Mana' : 25},
{'Nom' : 'Cape', 'Poids' : 1.1, 'Mana' : 13}]

poids_maximal = 4

def remplissage(liste_objet, poids_max):
    '''
    fonction qui ajoute dans la liste (liste_dans_malle) 
            les éléments dans l'ordre de (liste_objet)
            de sorte a ce que ca ne dépasse pas (poids_max)
    Entrée: liste de dictionnaires (liste_objet) et entier (poids_max) 
    Sortie: liste de dictionnaires des objet contenu dans la malle
    '''
    liste_dans_malle = []
    for objet in liste_objet:
        if poids_max - objet['Poids'] > 0:
            poids_max -= objet['Poids']
            liste_dans_malle.append(objet)
    return liste_dans_malle

def affichage_remplissage(liste):
    '''
    fonction qui affiche les éléments contenu dans la malle, et le poids de celle-ci
    Entrée: liste de dictionnaires (liste) 
    Sortie: un print de chaque élément de (liste) et le poids de la malle avec la valeur(poids_malle)
    '''
    poids_malle = 0
    for element in liste:
        print(f"dans la malle on peut mettre {element['Nom']}")
    for element in liste:
        poids_malle += element['Poids']
    print(f"la malle pèse {poids_malle} kg")

def remplissage_poids(liste_objet, poids_max):
    '''
    fonction qui trie (liste_objet) en fonction de la clée poids
             du plus grand au plus petit puis ajoute les élements de (liste_objet) trié
            dans la liste (liste_dans_malle2) de sorte à ce que ca ne dépasse pas (poids_malle)
    Entrée: liste de dictionnaires (liste_objet) et entier (poids_max) 
    sortie : liste de dictionnaires (liste_dans_malle2)
    '''
    liste_dans_malle2 = []
    for i in range(len(liste_objet) - 1):
        indice_grand = i
        for j in range(i + 1, len(liste_objet)) :
            if liste_objet[j]['Poids'] > liste_objet[indice_grand]['Poids']:
                indice_grand = j
        liste_objet[i]['Poids'], liste_objet[indice_grand]['Poids'] =liste_objet[indice_grand]['Poids'], liste_objet[i]['Poids']
    for objet in liste_objet:
        if poids_max - objet['Poids'] > 0:
            poids_max -= objet['Poids']
            liste_dans_malle2.append(objet)
    return liste_dans_malle2

def remplissage_mana(liste_objet,poids_max):
    '''
    fonction qui trie (liste_objet) en fonction de la clée mana 
            du plus grand au plus petit puis ajoute les élements de (liste_objet) trié 
            dans la liste (liste_dans_malle2) de sorte à ce que ca ne dépasse pas (poids_malle)
    Entrée: liste de dictionnaires (liste_objet) 
            et entier (poids_max) 
    sortie : liste de dictionnaires (liste_dans_malle3)
    '''
    liste_dans_malle3 = []
    for i in range(len(liste_objet) - 1):
        indice_grand = i
        for j in range(i + 1, len(liste_objet)) :
            if liste_objet[j]['Mana'] > liste_objet[indice_grand]['Mana']:
                indice_grand = j
        liste_objet[i], liste_objet[indice_grand] =liste_objet[indice_grand], liste_objet[i]
    print(liste_objet)
    for objet in liste_objet:
        if poids_max - objet['Poids'] > 0:
            poids_max -= objet['Poids']
            liste_dans_malle3.append(objet)
    return liste_dans_malle3

def affichage_mana(liste):
    '''
    fonction qui affiche les éléments contenu dans la malle, sa valeure magique avec le mana, et le poids de celle-ci
    Entrée: liste de dictionnaires (liste) 
    Sortie: un print de chaque élément de (liste),
             sa valeur magique avec (mana) 
             et le poids de la malle avec la valeur(poids_malle)
    '''
    poids_malle = 0
    mana = 0
    for element in liste:
        print(f"dans la malle on peut mettre {element['Nom']}")
    for element in liste:
        mana += element['Mana']
    print(f"la malle a une valeure magique de {mana}")
    for element in liste:
        poids_malle += element['Poids']
    print(f"la malle a un poids de  {poids_malle} kg")
    
def ihm():
    '''
    fonction qui permet un affichage clair du remplissage de la malle avec 3 choix proposés
            1 de remplir la malle dans l'ordre de la liste (fournitures_scolaires) 
            2 e remplir la malle  en essayant d'optimiser le poids de la malle
            3 de remplir la malle en essayant d'optimiser la mana de la malle 
    Entrée:
            un entier(int) 1 2 ou 3 
    Sortie:
            si le 1 a été saisi on a en sortie la fonctionn (affichage_remplissage) 
            prenant en conte la fonction (remplissage) 
            qui a pour arguments(fournitures_scolaires) et 4
            si le 2 a été saisi on a en sortie la fonctionn (affichage_remplissage) 
            prenant en conte la fonction (remplissage_poids) 
            qui a pour arguments(fournitures_scolaires) et 4
            si le 3 a été saisi on a en sortie la fonctionn (affichage_mana) 
            prenant en conte la fonction (remplissage_mana) 
            qui a pour arguments(fournitures_scolaires) et 4
            si le 4 c'est la fin du programe 
    '''
    choix = int(input("si vous voulez remplir la malle entrez 1, si vous voulez remplir la malle de façon optimisé en poids entrez 2 "
    "si vous voulez remplir la malle avec un maximum de mana entrez 3 "
    "ou si vous vouilez arreter de remplir la malle entrez 4 "))
    if choix == 1:
        affichage_remplissage(remplissage(fournitures_scolaires, 4))
        ihm()
    if choix == 2:
        affichage_remplissage(remplissage_poids(fournitures_scolaires, 4))
        ihm()
    if choix == 3:
        affichage_mana(remplissage_mana(fournitures_scolaires, 4))
        ihm()
    if choix == 4:
        print("merci d'avoir participer revenez remplir la malle une prochaine fois ")

def force_brut_liste_binaire(n):
    possibilite = []
    for i in range(2 ** n):
        nombre_binaire = bin(i)[2:]
        nombre_binaire = (n - len(nombre_binaire)) * '0' + nombre_binaire
        possibilite.append(nombre_binaire)
    return possibilite

def force_brut_liste_combinaisons(fourniture):
    nb_possiblilite = force_brut_liste_binaire(len(fourniture))
    liste_comb = []
    for possibilite in nb_possiblilite:
        combinaison = []
        for i, bit in enumerate(possibilite):
            if bit == '1':
                combinaison.append(fourniture[i])
        liste_comb.append(combinaison)
    return liste_comb
    
def force_brut_total_choix(liste_choix):
    tot_choix = []
    for possible in liste_choix:
        poids = 0.0
        total_mana = 0.0
        for cle, valeur in enumerate(possible):
            poids += float(valeur['Poids'])
            total_mana += float(valeur['Mana'])
        tot_choix.append([poids, total_mana])
    return tot_choix
    
def force_brut_meilleur_choix_poids(liste_choix, poids_max):
    meilleur_choix = [0, 0]
    meilleur_choix_list = []
    tot_choix = force_brut_total_choix(liste_choix)
    for i, choix in enumerate(tot_choix):
        if choix[0] < poids_max:
            if choix[0] > meilleur_choix[0]:
                meilleur_choix = choix
                meilleur_choix_list = liste_choix[i]
    return meilleur_choix, meilleur_choix_list

def force_brut_meilleur_choix_mana(liste_choix, poids_max):
    meilleur_choix = [0, 0]
    meilleur_choix_list = []
    tot_choix = force_brut_total_choix(liste_choix)
    for i, choix in enumerate(tot_choix):
        if choix[0] < poids_max:
            if choix[1] > meilleur_choix[1]:
                meilleur_choix = choix
                meilleur_choix_list = liste_choix[i]
    return meilleur_choix, meilleur_choix_list

ihm()


