# coding: utf-8
def fleury_et_bott(rendu_monnaie):
    '''
    ---------------------------------------------------
    Cette fonction permet de calculer un rendu monnaie 
    en billets et pieces chez Fleury & Bott.
    ----------------------------------------
    Entre --> Int
    Sortie --> List
    -----------------
    '''
    liste_argent = [['billet(s) de 500 euros', 500, 0],
                    ['billet(s) de 200 euros', 200, 0],
                    ['billet(s) de 100 euros', 100, 0],
                    ['billet(s) de 50 euros', 50, 0],
                    ['billet(s) de 20 euros', 20, 0],
                    ['billet(s) de 10 euros', 10, 0],
                    ['billet(s) de 5 euros', 5, 0],
                    ['pièce(s) de 2 euros', 2, 0],
                    ['pièce(s) de 1 euro', 1, 0]]
    for i in range(len(liste_argent)):
        while rendu_monnaie - liste_argent[i][1] >= 0:
            rendu_monnaie -= liste_argent[i][1]
            liste_argent[i][2] += 1
    return liste_argent


def madame_guipure(rendu_monnaie):
    '''
    -----------------------------------------------------------
    Cette fonction permet de calculer un rendu monnaie limiter
    en billets et pieces chez Madame Guipure 
    et egalement de calculer le surplus 
    qu'elle doit rendre si la valeur ne peut pas etre atteint.
    -----------------------------------------------------------
    Entre --> Int
    Sortie --> List et Boole
    ----------------
    '''
    liste_argent = [['billet(s) de 200 euros', 200, 0, 1], ['billet(s) de 100 euros', 100, 0, 3],
                    ['billet(s) de 50 euros', 50, 0, 1], ['billet(s) de 20 euros', 20, 0, 1],
                    ['billet(s) de 10 euros', 10, 0, 1], ['pièce(s) de 2 euros', 2, 0, 5]]
    if rendu_monnaie > 590:
        for i in range(len(liste_argent)):
            while (rendu_monnaie - liste_argent[i][1] >= 0) and \
                  (liste_argent[i][3] > 0):
                rendu_monnaie -= liste_argent[i][1]
                liste_argent[i][2] += 1
                liste_argent[i][3] -= 1
        liste_secour = fleury_et_bott(rendu_monnaie)
        for argent in liste_secour:
            if argent[2] > 0:
                liste_argent.append(argent)
        return liste_argent, False
    elif rendu_monnaie > 0:
        for i in range(len(liste_argent)):
            while (rendu_monnaie - liste_argent[i][1] >= 0) and \
                  (liste_argent[i][3] > 0):
                rendu_monnaie -= liste_argent[i][1]
                liste_argent[i][2] += 1
                liste_argent[i][3] -= 1
        if rendu_monnaie > 0:
            plus_proche = 500
            for rendu_eventuel in liste_argent:
                if (rendu_eventuel[3] > 0) and (plus_proche > rendu_monnaie):
                    plus_proche = rendu_eventuel[1]
            return liste_argent, True
    return liste_argent, False


def ollivander(rendu_monnaie):
    '''
    --------------------------------------------------
    Cette fonction permet de calculer le rendu monnaie 
    en monnaie de sorcier chez Ollivander's 
    en le convertissant en Noises 
    puis en le repartissant a nouveau.
    ----------------------------------
    Entre --> Int
    Sortie --> List
    ----------------
    '''
    liste_argent_sorcier = [[' Gallion(s), ', 493, 0],
                            [' Mornille(s), ', 29, 0],
                            [' Noise(s) ', 1, 0]]
    nombre_noise = liste_argent_sorcier[0][1] * rendu_monnaie[0]\
    + liste_argent_sorcier[1][1] * rendu_monnaie[1]\
    + liste_argent_sorcier[2][1] * rendu_monnaie[2]
    for i in range(len(rendu_monnaie)):
        liste_argent_sorcier[i][2] = nombre_noise //\
            liste_argent_sorcier[i][1]
        nombre_noise %= liste_argent_sorcier[i][1]
    return liste_argent_sorcier


def affichage1(liste_des_sommes_a_rendre, probleme):
    '''
    --------------------------------------
    Cette fonction permet d'afficher dans
    le terminal les rendus des fonctions
    Guipure et Fleury_et_bott
    et de renvoyer 'Rien'
    si la valeur est egale a 0.
    -----------------------------
    Entre --> Sois List sois Int
    Sortie --> List
    -----------------------------
    '''
    somme = 0
    for nombre in liste_des_sommes_a_rendre:
        somme += nombre[2]
    if somme == 0:
        print('Rien')
    else:
        for billet in liste_des_sommes_a_rendre:
            if billet[2] > 0:
                print(f"IL faut rendre {billet[2]} {billet[0]} ")       
        if probleme == True:
            print("\nMADAME GUIPURE NE PEUT PAS REMBOURSER TOUT L'ARGENT, "
                  "ELLE REND PLUS. ")
    return


def affichage2(liste_des_sommes_a_rendre_ollivanders):
    '''
    ----------------------------------------------------------
    Cette fonction permet d'afficher dans
    le terminal les rendus de la fontion Ollivander
    et de renvoyer un message négatif si on ne peut pas rendre
    la monnaie car il n'y a pas assez de noises 
    pour faire la piece superieur.
    ------------------------------
    Entre --> List
    Sortie --> List
    ----------------
    '''
    for monnaie in liste_des_sommes_a_rendre_ollivanders:
        if monnaie[2]> 0:
            print(f"Mr. Ollivander va vous rendre {monnaie[2]}{monnaie[0]}")
    if monnaie[2] == 0:
        print("Le rendu monnaie est impossible")
    return
    
        
def procedure_menu():
    '''
    --------------------------------------
    Cette fonction permet de crée le menu 
    dans le terminal et d'interagir avec.
    --------------------------------------
    '''
    print("(Re)Bienvenue au chemin de Traverse.")
    choix_fleury_et_bott = ''
    choix_guipure = ''
    choix_ollivander = ''
    choix_boutique = input("\nSi vous voulez acheter des livres, allez à la boutique de Fleury & Bott et tapez donc | Fleury | ou | 1 |. \n"
    "Si vous voulez plutôt avoir une belle robe, dirigez-vous chez Madame Guipure et tapez donc | Guipure | ou | 2 |. \n"
    "Ou encore vous préférerez aller chercher votre baguette qui n'attends que vous, présentez-vous chez Ollivander's et tapez donc | Ollivander | Ou | 3 |. \n"
    "Si votre visite dans le chemin est terminée, tapez | Quitter | ou | 4 |. \n")
       
    if choix_boutique in ['Fleury', '1']:
        print("\nBienvenue chez Fleury et Bott.")
        while choix_fleury_et_bott not in ['Sortir', '3']:    
            choix_fleury_et_bott = input("\nSi vous voulez voir les sommes à rendres suggéré "
            "tapez | Valeurs | ou | 1 |. \n"
            "Ou alors si vous voulez tester une valeur par vous même tapez | Test | ou | 2 |. \n"
            "Si jamais vos achats sont terminés tapez | Sortir | ou | 3 |. \n")
            if choix_fleury_et_bott in ['Valeurs', '1']:
                liste_fleury_et_bott = [0, 60, 63, 231, 899]
                for i in range(len(liste_fleury_et_bott)):
                    print(f"\nPour rendre une somme de {liste_fleury_et_bott[i]} "
                    "euro(s), le gérant vous rendra :")
                    affichage1(fleury_et_bott(liste_fleury_et_bott[i]),False)
            elif choix_fleury_et_bott in ['Test', '2']:
                valeur_fleury_et_bott = int(input("Quel somme voulez-vous qu'on vous rende : "))
                if valeur_fleury_et_bott < 0:
                    print("Il faut une valeur positive s'il vous plaît.")
                else:
                    print(f"\nPour rendre une somme de {valeur_fleury_et_bott} euro(s) il faut : ")
                    affichage1(fleury_et_bott(valeur_fleury_et_bott),False)
            elif choix_fleury_et_bott in ['Sortir', '3']:
                print("\nLe gérant du magasin vous remercie et vous invite a passez chez ses voisins. \n")
                procedure_menu()
       
    elif choix_boutique in ['Guipure', '2']:
        print("\nBienvenue chez Madame Guipure")
        while choix_guipure not in ['Sortir', '3']:    
            choix_guipure = input("\nSi vous voulez voir les sommes à rendres suggéré "
                "tapez | Valeurs | ou | 1 |. \n"
                "Ou alors si vous voulez tester une valeur par vous même tapez | Test | ou | 2 |. \n"
                "Si jamais vos achats sont terminés tapez | Sortir | ou | 3 |. \n")
            if choix_guipure in ['Valeurs', '1']:
                liste_guipure = [0, 8, 62, 231, 497, 842]
                for somme in liste_guipure:
                    print(f"\nPour rendre une sommme de {somme} euro(s), Madame Guipure vous rendra :")
                    liste_resultat_guipure, soucis = madame_guipure(somme)
                    affichage1(liste_resultat_guipure, soucis)
                    if somme > 590 :
                        print("\nMADAME GUIPURE NE PEUT PAS REMBOURSER TOUT L'ARGENT, "
                              "ELLE A EMPRUNTE CHEZ FLEURY ET BOTT.")
            elif choix_guipure in ['Test', '2']:
                valeur_guipure = int(input("Quel somme voulez-vous qu'on vous rende : "))
                if valeur_guipure < 0:
                    print("Il faut une valeur positive s'il vous plaît.")
                else:
                    print(f"Pour rendre une sommme de {valeur_guipure} euro(s) il faut : ")
                    affichage1(madame_guipure(valeur_guipure)[0],madame_guipure(valeur_guipure)[1])
            elif choix_guipure in ['Sortir', '3']:
                print("\nMadame Guipure vous remercie et vous invite a passez chez ses voisins. \n")
                procedure_menu()
    
    elif choix_boutique in ['Ollivander', '3']:
        print("\nBienvenue chez Ollivander's")
        while choix_ollivander not in ['Sortir', '3']:    
            choix_ollivander = input("\nSi vous voulez voir les sommes à rendres suggéré "
                "tapez | Valeurs | ou | 1 |. \n"
                "Ou alors si vous voulez tester une valeur par vous même tapez | Test | ou | 2 |. \n"
                "Si jamais vos achats sont terminés tapez | Sortir | ou | 3 |. \n")
            if choix_ollivander in ['Valeurs', '1']:
                nb_a_tester = [[0, 0, 0] , [0, 0, 654] , [0, 23, 78] , [2, 11, 9] , [7, 531, 451]]
                for i in range(len(nb_a_tester)):
                    print(f"\nPour les valeurs de {nb_a_tester[i][0]} Gallion(s)"
                    f", {nb_a_tester[i][1]} Mornille(s) et {nb_a_tester[i][2]} "
                    " Noise(s) :")
                    affichage2(ollivander(nb_a_tester[i]))
            elif choix_ollivander in ['Test', '2']:
                liste_valeur_olivander = [0, 0, 0]
                print("Vous allez demander 3 valeurs")
                gallions = int(input("Quel est le nombre de Gallion(s) :"))
                mornilles = int(input("Quel est le nombre de Mornille(s) :"))
                noises = int(input("Quel est le nombre de Noise(s):"))
                liste_valeur_olivander[0] = gallions
                liste_valeur_olivander[1] = mornilles
                liste_valeur_olivander[2] = noises
                print(f"\nPour votre demande :")
                affichage2(ollivander(liste_valeur_olivander))
            elif choix_ollivander in ['Sortir', '3']:
                print("\nMonsieur Ollivander vous remercie et vous invite a passez chez ses voisins. \n")
                procedure_menu()

    if choix_boutique in ['Quitter', '4']:
        print("\nSatisfait de vos achats ? Revenez bientôt !")
    return

procedure_menu()#la fonction de l'IHM est utlise